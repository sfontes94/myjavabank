package org.academiadecodigo.javabank.menu;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.domain.account.AccountType;
import org.academiadecodigo.javabank.managers.AccountManager;

public class Menu {

    //-------------------------COLORS-----------------------------
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUD = "\u001B[42m";

    // TITLE MENU COLOR
    public static final String ANSI_PURPLE_COLOR = "\u001B[35m";

    // RESET COLOR
    public static final String ANSI_RESET = "\u001B[0m";

    private Boolean isLogged = false;
    int id = 1;
    private AccountManager accountManager;
    private Customer customer;


    //-------------------------Main Menu---------------------------
    // Options
    String[] mainMenuOptions = {
            "Enter Account",
            "Create an Account",
            "Exit"
    };

    public Menu() {
    }

    public int mainMenu(){
        Prompt promptMain = new Prompt(System.in, System.out);
        MenuInputScanner mainMenuScanner = new MenuInputScanner(mainMenuOptions);
        mainMenuScanner.setMessage(ANSI_PURPLE_COLOR +
                                    "Welcome to our Java Bank !! \n " +
                                    "Choose an option:" +
                                    ANSI_RESET);

        int mainMenuInput = promptMain.getUserInput(mainMenuScanner);
        return mainMenuInput;
    }


    int mainInput = mainMenu();


    //-------------------------Login/Logout----------------------------
    // Options
    String[] loginMenuOptions = {"Username: "};

    public void logging(){
        Prompt loginPrompt = new Prompt(System.in, System.out);
        MenuInputScanner usernameScanner = new MenuInputScanner(loginMenuOptions);
        usernameScanner.setMessage(ANSI_PURPLE_COLOR +
                "Please enter your username: " +
                ANSI_RESET);
        int usernameInput = loginPrompt.getUserInput(usernameScanner);


                if(usernameInput == id){ // Check id (valid/invalid)
                    isLogged = true;
                    System.out.println(ANSI_GREEN_BACKGROUD +
                            "Login successful!" +
                            ANSI_RESET + "\n");

                    System.out.println(ANSI_PURPLE_COLOR +
                            "Welcome Home !" +
                            ANSI_PURPLE_COLOR);


                } else { // If the username is invalid
                    System.out.println(ANSI_RED_BACKGROUND +
                            "Username not found or password incorrect."
                            + ANSI_RESET);
                    mainMenu();
                }

    }


    //-------------------------DEPOSIT----------------------------

    public void depositOption(){
                Prompt depositPrompt = new Prompt(System.in, System.out);
                IntegerInputScanner depositScanner = new IntegerInputScanner();
                depositScanner.setMessage(ANSI_PURPLE_COLOR +
                        "Amount to Deposit: " +
                        ANSI_RESET);
                Integer userDepositInput = depositPrompt.getUserInput(depositScanner);



                Prompt depositPrompt2 = new Prompt(System.in, System.out);
                StringInputScanner depositScanner2 = new StringInputScanner();
                depositScanner2.setMessage(ANSI_PURPLE_COLOR +
                                            "Confirm deposit? \n"
                                            + userDepositInput + "\n Yes/No \n" +
                                            ANSI_RESET); // Confirm deposit
                String userDepositInput2 = depositPrompt2.getUserInput(depositScanner2);

                if (userDepositInput2.equals("yes")){
                    accountManager.deposit(id, userDepositInput.doubleValue());
                } else {
                    mainMenu();
                }
    }


    //---------------------------WITHDRAW------------------------------

    public void withdraw(){
            Prompt withdrawPrompt = new Prompt(System.in, System.out);
            IntegerInputScanner withdrawScanner = new IntegerInputScanner();
            withdrawScanner.setMessage(ANSI_PURPLE_COLOR +
                    "Amount to Withdraw: " +
                    ANSI_RESET);
            Integer userwithdrawInput = withdrawPrompt.getUserInput(withdrawScanner);



            Prompt withdrawPrompt2 = new Prompt(System.in, System.out);
            StringInputScanner withdrawScanner2 = new StringInputScanner();
            withdrawScanner2.setMessage(ANSI_PURPLE_COLOR +
                                        "Confirm withdraw? \n" +
                                         userwithdrawInput + "\n Yes/No \n" +
                                        ANSI_RESET); // Confirm deposit
            String userwithdrawInput2 = withdrawPrompt2.getUserInput(withdrawScanner2);

            if (userwithdrawInput2.equals("yes")){
                accountManager.withdraw(id, userwithdrawInput.doubleValue());
            } else {
                mainMenu();
            }
        }



    //---------------------------DEBIT------------------------------

    public void debit(){
            Prompt debitPrompt = new Prompt(System.in, System.out);
            IntegerInputScanner debitScanner = new IntegerInputScanner();
            debitScanner.setMessage(ANSI_PURPLE_COLOR +
                    "Amount to Debit: " +
                    ANSI_RESET);
            Integer userDebitInput = debitPrompt.getUserInput(debitScanner);



            Prompt debitPrompt2 = new Prompt(System.in, System.out);
            StringInputScanner debitScanner2 = new StringInputScanner();
            debitScanner2.setMessage(ANSI_PURPLE_COLOR +
                    "Confirm Debit? \n" +
                    userDebitInput + "\n Yes/No" +
                    ANSI_RESET); // Confirm deposit
            String userDebitInput2 = debitPrompt2.getUserInput(debitScanner2);

            if (userDebitInput2.equals("yes \n")){
                //DEBIT;
                mainMenu(); // CHANGE THIS
            } else {
                mainMenu();
            }
        }


    //---------------------------TRANSFER------------------------------

    public void transfer(){
        // ASK AMOUNT
        Prompt transferPrompt = new Prompt(System.in, System.out);
        IntegerInputScanner transferScanner = new IntegerInputScanner();
        transferScanner.setMessage(ANSI_PURPLE_COLOR +
                "Amount to Transfer: " +
                ANSI_RESET);
        Integer userTransferInput = transferPrompt.getUserInput(transferScanner);

        // ASK DESTINATION ACCOUNT
        Prompt transferPrompt2 = new Prompt(System.in, System.out);
        IntegerInputScanner transferScanner2 = new IntegerInputScanner();
        transferScanner2.setMessage(ANSI_PURPLE_COLOR +
                "To which Account id number? " +
                ANSI_RESET);
        Integer idDestin = transferPrompt2.getUserInput(transferScanner2);

        // CONFIRN
        Prompt transferPrompt3 = new Prompt(System.in, System.out);
        StringInputScanner debitScanner3 = new StringInputScanner();
        debitScanner3.setMessage(ANSI_PURPLE_COLOR +
                "Confirm Transfer? \n" +
                userTransferInput + "\n Yes/No \n" +
                ANSI_RESET); // Confirm deposit
        String userwithdrawInput3 = transferPrompt3.getUserInput(debitScanner3);

        if (userwithdrawInput3.equals("yes")){
            accountManager.transfer(id, idDestin, userTransferInput);
        } else {
            mainMenu();
        }
    }





    //---------------------After is logged, select Option-------------------

    String[] accountOptions = {"deposit", "Withdraw", "debit", "transfer", "Exit"};

    public void inAccount(){
        if(isLogged) { // If logged, open account options
                        Prompt accountPrompt = new Prompt(System.in, System.out);
                        MenuInputScanner accountScanner = new MenuInputScanner(accountOptions);
                        accountScanner.setMessage(ANSI_PURPLE_COLOR +
                                "Please select one of the following options: " +
                                ANSI_RESET);

                        int userAccountInput = accountPrompt.getUserInput(accountScanner);

        if(userAccountInput != 5){

             if(userAccountInput == 1) { // DEPOSIT CASH
                depositOption();
             }

             if(userAccountInput == 2){
                 withdraw();
             }

             if(userAccountInput == 3){
                debit();
             }
             if(userAccountInput == 4){
                 transfer();
             }

        }
        mainMenu();
        }
    }

    String[] accountType = {"Savings Account", "Checking Account"};
    public void createAccount(){ // CHOOSE ACCOUNT TYPE
            Prompt openPrompt = new Prompt(System.in, System.out);
            MenuInputScanner openScanner = new MenuInputScanner(accountType);
            openScanner.setMessage(ANSI_PURPLE_COLOR +
                    "Which Account would you like to create ? \n" +
                    "Please select one of the following options \n" +
                    ANSI_RESET);
            Integer userOpenInput = openPrompt.getUserInput(openScanner);

            if(!userOpenInput.toString().equals("quit")) {
                if (userOpenInput.equals(1)) {
                    accountManager.openAccount(AccountType.SAVINGS);
                    openScanner.setMessage(ANSI_GREEN_BACKGROUD +
                            "Savings Account Created !" +
                            ANSI_RESET);
                    mainMenu();
                } else if (userOpenInput.equals(2)) {
                    accountManager.openAccount(AccountType.CHECKING);
                    openScanner.setMessage(ANSI_GREEN_BACKGROUD +
                            "Checking Account Created !" +
                            ANSI_RESET);
                }
            }
            mainMenu();


    }




    //-------------------------Join All Methods----------------------------
    public void startMenu(){

        while(mainInput != 3){
            if(mainInput == 1){ // Enter Account
                logging();
                inAccount();
            }
            if(mainInput == 2){
                createAccount();

            }
        }
    }

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.startMenu();
    }



    // debit
    // credit
    // withdraw


}





